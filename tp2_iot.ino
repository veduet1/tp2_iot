/*
  Lora Send And Receive
  This sketch demonstrates how to send and receive data with the MKR WAN
  1300/1310 LoRa module. This example code is in the public domain.
*/
#include "src/mkrwan/MKRWAN.h"
#include "src/mkrwan/utilities.h"

#include "src/temperature_packet.hpp"

#include <Adafruit_SHT31.h>

#define MAGIC_NUMBER 0x5D

LoRaModem modem;
Adafruit_SHT31 sht31;

void blinkError() {
  while (1) {
    digitalWrite(LED_BUILTIN, 1);
    delay(1000);
    digitalWrite(LED_BUILTIN, 0);
    delay(1500);
  }
}

// key[16] cotain 16 byte key(128 bit) for encryption
uint8_t key[16] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                   0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F};

// concaténation entre DEVEUI et APPEUI    
uint8_t iv[16] = {0};

TemperaturePacket packet;

void retrieveCredentials() {
  while (1) {
    print("DEVEUI > ");

    const String deviceEUI = Serial.readStringUntil('\n');

    if (!checkHexaStr(deviceEUI, 8)) {
      print("Error : Invalid DEVEUI `", deviceEUI, "`");
    } else {
      modem.setDeviceEui(deviceEUI);
      break;
    }
  }

  while (1) {
    print("APPEUI > ");

    const String appEui = Serial.readStringUntil('\n');

    if (!checkHexaStr(appEui, 8)) {
      print("Error : Invalid APPEUI `", appEui, "`");
    } else {
      modem.setAppEui(appEui);
      break;
    }
  }

  while (1) {
    print("APPKEY > ");

    const String appKey = Serial.readStringUntil('\n');

    if (!checkHexaStr(appKey, 16)) {
      print("Error : Invalid APPKEY `", appKey, "`");
    } else {
      modem.setAppKey(appKey);
      break;
    }
  }
}

bool fill_half(char val, uint8_t &receptor, bool msb) {
  uint8_t tmp = 0;

       if (val >= '0' && val <= '9') tmp = static_cast<uint8_t>(val - '0');
  else if (val >= 'a' && val <= 'f') tmp = static_cast<uint8_t>(val - 'a') + 0x0A;
  else if (val >= 'A' && val <= 'F') tmp = static_cast<uint8_t>(val - 'A') + 0x0A;
  else return false;

  receptor |= (msb)? tmp << 4 : tmp;

  return true;
}

size_t fillBytes(String &input, uint8_t *output, size_t output_len) {
  if (output == nullptr) return 0;

  size_t   str_len = input.length();
  size_t input_len = (str_len / 2) + (str_len % 2);

  size_t  in_idx = 0;
  size_t out_idx = 0;

  if (str_len % 2) {
    uint8_t byte = 0;

    if (!fill_half(input[0], byte, false)) return 0;

    output[0] = byte;
    
    in_idx = 1;
    out_idx = 1;
  }

  for (; out_idx < input_len && out_idx < output_len; ++out_idx) {
    uint8_t byte = 0;

    if (!fill_half(input[in_idx],     byte,  true)) return out_idx;
    if (!fill_half(input[in_idx + 1], byte, false)) return out_idx;

    output[out_idx] = byte;

    in_idx += 2;
  }

  return out_idx;
}

void fillCipher() {
  String tmp = modem.getDevEUI();

  fillBytes(tmp, &iv[0], 8);

  tmp = modem.getAppEUI();

  fillBytes(tmp, &iv[8], 8);
}

void setup() {
  // Initializing serial
  Serial.begin(115200);
  Serial.setTimeout(10000);
  while (!Serial) delay(10);

  pinMode(LED_BUILTIN, OUTPUT);

  Serial.println("SHT31 test");
  if (!sht31.begin(0x44)) { // Set to 0x45 for alternate i2c addr
    Serial.println("Couldn't find SHT31");
    blinkError();
  }

  // change this to your regional band (eg. US915, AS923, ...)
  if (!modem.begin(LoRaBand::EU868)) {
    debug("Error: Failed to start module");

    blinkError();
  };

  debug("\n\n\n");
  debug("Your module version is: ", modem.version());
  debug("Dev EUI: ", modem.getDevEUI());
  debug("App EUI: ", modem.getAppEUI());
  debug("App Key: ", modem.getAppKey());
  debug("\n\n\n");

  char hint;

  if (!modem.readFromNVM(hint)) {
    debug("Error: Unable to read hint from NVM");

    blinkError();
  }

  if (hint != MAGIC_NUMBER) {
    retrieveCredentials();

    hint = MAGIC_NUMBER;

    if (!modem.writeToNVM(hint)) {
      debug("Error: Unable to write NVM");
    }
  }

  int connected = modem.joinOTAA();
  if (!connected) {
    print("Something went wrong; are you indoor? Move near a window and retry");

    blinkError();
  }

  fillCipher();

  // Set poll interval to 60 secs.
  modem.minPollInterval(60);

  packet.setCtrParams(key, 16, iv, 16);
}

void loop() {
  int err;
  const size_t packet_length = packet.getLength();
  uint8_t data[packet_length] = {0};

  float t = sht31.readTemperature();
  packet.setTemperature(t);

  packet.fillBuffer(data, packet_length);

  modem.beginPacket();

  modem.write(data, packet_length);

  err = modem.endPacket(true);

  if (err > 0) {
    print("Message sent correctly!");
    // if message send -> compteur++
    // incrémentation du compteur (il faut manipule le tableau d'octet)
  } else {
    print("Error sending message :(");
    print("(you may send a limited amount of messages per minute, depending on "
          "the signal strength");
    print("it may vary from 1 message every couple of seconds to 1 message "
          "every minute)");
  }

  delay(60000);
}
