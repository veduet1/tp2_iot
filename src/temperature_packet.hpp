#ifndef TEMPERATURE_PACKET_HPP_INCLUDED
#define TEMPERATURE_PACKET_HPP_INCLUDED

#include "packet/packet.hpp"

class TemperaturePacket : public Packet {
    float temperature;

protected:
    uint16_t bufferLength() const final;
    void fillRaw(uint8_t *buffer) const final;

public:
    void setTemperature(float temperature);
    float getTemperature() const;
};

#endif // TEMPERATURE_PACKET_HPP_INCLUDED