#include "convert.hpp"

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
void u16_to_bytes_be(uint16_t from, uint8_t *buffer) {
    uint8_t *raw = reinterpret_cast<uint8_t *>(&from);

    buffer[0] = raw[1];
    buffer[1] = raw[0];
}

void u32_to_bytes_be(uint32_t from, uint8_t *buffer) {
    uint8_t *raw = reinterpret_cast<uint8_t *>(&from);

    buffer[0] = raw[3];
    buffer[1] = raw[2];
    buffer[2] = raw[1];
    buffer[3] = raw[0];
}

void float_to_bytes_be(float from, uint8_t *buffer) {
    uint8_t *raw = reinterpret_cast<uint8_t *>(&from);

    buffer[0] = raw[3];
    buffer[1] = raw[2];
    buffer[2] = raw[1];
    buffer[3] = raw[0];
}
#else
void u16_to_bytes_be(uint16_t from, uint8_t *buffer) {
    uint8_t *raw = reinterpret_cast<uint8_t *>(&from);

    buffer[0] = raw[0];
    buffer[1] = raw[1];
}

void u32_to_bytes_be(uint32_t from, uint8_t *buffer) {
    uint8_t *raw = reinterpret_cast<uint8_t *>(&from);

    buffer[0] = raw[0];
    buffer[1] = raw[1];
    buffer[2] = raw[2];
    buffer[3] = raw[3];
}

void float_to_bytes_be(float from, uint8_t *buffer) {
    uint8_t *raw = reinterpret_cast<uint8_t *>(&from);

    buffer[0] = raw[0];
    buffer[1] = raw[1];
    buffer[2] = raw[2];
    buffer[3] = raw[3];
}
#endif
