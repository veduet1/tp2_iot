#include "packet.hpp"
#include "convert.hpp"

#include <assert.h>

constexpr size_t HEADER_LENGTH = 8;

void Packet::setCtrParams(uint8_t *key, size_t key_length, uint8_t *cipher, size_t cipher_length) {
    ctr.setKey(key, key_length);
    ctr.setIV(cipher, cipher_length);
    ctr.setCounterSize(sizeof(counter));
}

void Packet::fillHeader(uint8_t *buffer) {
    u32_to_bytes_be(   counter,   &buffer[0]);
    u32_to_bytes_be(crc.getCRC(), &buffer[4]);
}

void Packet::fillBody(uint8_t *buffer) {
    fillRaw(buffer);
    ctr.encrypt(buffer, buffer, bufferLength());
    crc.add(buffer, bufferLength());
}

size_t Packet::fillBuffer(uint8_t *buffer, size_t buffer_length) {
    if (buffer == nullptr) return 0;

    const size_t total_length = HEADER_LENGTH + bufferLength();
    
    if (buffer_length < total_length) return 0;

    crc.reset();

    fillBody(&buffer[HEADER_LENGTH]);

    fillHeader(buffer);

    const uint32_t remaining = 16 - bufferLength() % 16;
    counter += bufferLength() / 16;

    if (remaining != 16) {
        uint8_t dummy[16] = { 0 };

        ctr.encrypt(dummy, dummy, remaining);

        counter ++;
    }

    return total_length;
}