#ifndef PAQUET_HPP_INCLUDED
#define PAQUET_HPP_INCLUDED

#include <cstdint>
#include <cstdlib>

#include "AES.h"
#include "CTR.h"
#include "CRC32.h"

// Format du paquet:
//           0                   1                   2                   3
//           0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 
//          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   0x0000 |                            COUNTER                            |
//          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   0x0020 |                            CRC 32                             |
//          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   0x0040 |                                                               |
//          |                             DATA                              |
// - 0x0040 |                                                               |
//          +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//
class Packet {
    CTR<AES128> ctr;
    uint32_t counter;
    CRC32 crc;

    void fillHeader(uint8_t *buffer);
    void fillBody(uint8_t *buffer);
protected:
    
    virtual uint16_t bufferLength() const = 0;
    virtual void fillRaw(uint8_t *buffer) const = 0;

public:
    void setCtrParams(uint8_t *key, size_t key_length, uint8_t *cipher, size_t cipher_length);

    inline size_t getLength() const { return 8 + bufferLength(); }
    inline size_t getCounter() const { return counter; }

    size_t fillBuffer(uint8_t *buffer, size_t buffer_length);
};


#endif // PAQUET_HPP_INCLUDED