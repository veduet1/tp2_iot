#ifndef CONVERT_HPP_INCLUDED
#define CONVERT_HPP_INCLUDED

#include <cstdint>

void u16_to_bytes_be(uint16_t from, uint8_t *buffer);
void u32_to_bytes_be(uint32_t from, uint8_t *buffer);

void float_to_bytes_be(float from, uint8_t *buffer);

#endif // CONVERT_HPP_INCLUDED