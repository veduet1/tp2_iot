#include "temperature_packet.hpp"

#include "packet/convert.hpp"

void TemperaturePacket::setTemperature(float temperature) {
    this->temperature = temperature;
}

float TemperaturePacket::getTemperature() const {
    return temperature;
}

uint16_t TemperaturePacket::bufferLength() const {
    return sizeof(float);
}

void TemperaturePacket::fillRaw(uint8_t *buffer) const {
    float_to_bytes_be(temperature, buffer);
}
